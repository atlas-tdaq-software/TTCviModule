#include <iostream>
#include <unistd.h>
#include <stdlib.h>
#include <fstream>

#include "DFSubSystemItem/Config.h"
#include "config/ConfigObject.h"
#include "config/Configuration.h"
#include "dal/Partition.h"
#include "dal/RunControlApplication.h"
#include "dal/ResourceBase.h"

#include "RCDTtc/RCDTtcvi.h"
#include "TTCviModule/TTCviModule.h"

#include "TTCvidal/TTCviModule.h"
#include "TTCvidal/TTCviBGoChannelBase.h"
#include "TTCvidal/TTCviBGoChannel.h"
#include "TTCvidal/TTCviBCR.h"
#include "TTCvidal/TTCviECR.h"
#include "TTCvidal/TTCviCycleBase.h"
#include "TTCvidal/TTCviCycleShort.h"
#include "TTCvidal/TTCviCycleLong.h"

#include "RunControl/Common/OnlineServices.h"

#include "ers/ers.h"
#include "TTCInfo/TTCVI_ISNamed.h"

using namespace ROS;
using namespace RCD;
using namespace std;

//------------------------------------------------------------------------------
TTCviModule::TTCviModule() :
  m_status(0),
  m_ipcpartition(getenv("TDAQ_PARTITION")),
  m_partition(nullptr),
  m_confDB(nullptr),
  m_configuration(),
  m_dbModule(nullptr),
  m_UID(""),
  m_rcd_uid(""),
  m_ttcvi(nullptr),
  m_PhysAddress(0),
  m_OperationMode(""),
  m_OrbitInput(0),
  m_CounterSelection(0),
  m_L1AInput(0),
  m_L1ARandomGeneratorFrequency(0),
  m_TriggerWordEnable(false),
  m_TriggerWordExternal(false),
  m_TriggerWordAddress(0),
  m_TriggerWordSubaddress(0),
  m_IS_server(""),
  m_useConfiguration(false),
  m_useMonitoring(false)
{
  std::string sout = m_UID + "@TTCviModule::TTCviModule() ";
  ERS_LOG(sout << "Entered");
  
  this->resetConfiguration();

  ERS_LOG(sout << "Done");
}

TTCviModule::~TTCviModule() noexcept
{
  std::string sout = m_UID + "@TTCviModule::~TTCviModule() ";
  ERS_LOG(sout << "Entered");

  ERS_LOG(sout << "Done");
}

void TTCviModule::setup(DFCountedPointer<Config> configuration)
{
  std::string sout = "TTCviModule::setup() ";
  ERS_LOG(sout << " Entered");

  ERS_LOG(sout << " m_ipcpartition = " << m_ipcpartition.name());

  m_configuration = configuration;


  this->resetConfiguration();

  m_UID = m_configuration->getString("UID");
  sout = m_UID + "@TTCviModule::setup() ";

  m_rcd_uid = m_configuration->getString("appName");
  ERS_LOG(sout << "m_rcd_uid = " << m_rcd_uid);

  try {
    m_confDB = m_configuration->getPointer<Configuration>("configurationDB");
  } catch (CORBA::SystemException& ex) {
    ostringstream text;
    text << sout << "Exception caught while creating configurationDB: " << ex._name();
    ERS_REPORT_IMPL( ers::warning, ers::Message, text.str(), );
  } catch (std::exception& ex) {
    ostringstream text;
    text << sout << "Exception caught while creating configurationDB: " << ex.what();
    ERS_REPORT_IMPL( ers::warning, ers::Message, text.str(), );
  } catch (...) {
    ostringstream text;
    text << sout << "Unknown Exception caught while creating configurationDB, rethrown";
    ERS_REPORT_IMPL( ers::warning, ers::Message, text.str(), );
    throw;
  }

  // get partition object
  m_partition= const_cast<daq::core::Partition*>(&daq::rc::OnlineServices::instance().getPartition());
  m_dbModule = const_cast<TTCvidal::TTCviModule*>(m_confDB->get<TTCvidal::TTCviModule>(m_UID));

  // First get attributes from the module:
  m_PhysAddress = m_configuration->getUInt("PhysAddress");
  ERS_LOG(sout << "m_PhysAddress = 0x" << std::hex << m_PhysAddress << std::dec);

  m_IS_server = m_configuration->getString("IS_server");
  ERS_LOG(sout << "m_IS_server = " << m_IS_server);

  m_OperationMode = m_configuration->getString("OperationMode");
  ERS_LOG(sout << "m_OperationMode = " << m_OperationMode);
  if (m_OperationMode == "configure-and-monitoring") {
    m_useConfiguration = true;
    m_useMonitoring = true;
  }
  else if (m_OperationMode == "configure-only") {
    m_useConfiguration = true;
    m_useMonitoring = false;
  }
  else if (m_OperationMode == "monitoring-only") {
    m_useConfiguration = false;
    m_useMonitoring = true;
  }
  else {
    ;
  }
  ERS_LOG(sout << "m_useConfiguration = " << m_useConfiguration);
  ERS_LOG(sout << "m_useMonitoring = " << m_useMonitoring);

  if (m_useConfiguration) {
    // Get TTCvi configuration
    const TTCvidal::TTCviConfigBase* ttcviConfBase = m_dbModule->get_Configuration();
    
    // if TTCviConfig
    if (const TTCvidal::TTCviConfig* ttcviconf = m_confDB->cast<TTCvidal::TTCviConfig>(ttcviConfBase)) {
      ERS_LOG(sout << "Found TTCviConfig");
      this->setup_TTCviConfig(ttcviconf);
    } 
    // else if TTCviSimpleConfig
    else if (const TTCvidal::TTCviSimpleConfig* ttcviconf = m_confDB->cast<TTCvidal::TTCviSimpleConfig>(ttcviConfBase)) {
      ERS_LOG(sout << "Found TTCviSimpleConfig");
      this->setup_TTCviSimpleConfig(ttcviconf);
      
    } else {   // else: no configuration
      ostringstream text;
      text << sout << "No TTCviConfig provided in configuration database, but the TTCvi is supposed to be configred";
      ERS_REPORT_IMPL( ers::warning, ers::Message, text.str(), );
    } 

    ERS_LOG(sout << ">>> Configuration Summary <<<" << std::endl);
    ERS_LOG(sout << "m_OrbitInput =" << m_OrbitInput );
    ERS_LOG(sout << "m_CounterSelection = " << m_CounterSelection );
    ERS_LOG(sout << "m_L1AInput = " << m_L1AInput );
    ERS_LOG(sout << "m_L1ARandomGeneratorFrequency = " << m_L1ARandomGeneratorFrequency );
    ERS_LOG(sout << "m_TriggerWordEnable     = " << m_TriggerWordEnable);
    ERS_LOG(sout << "m_TriggerWordExternal   = " << m_TriggerWordExternal);
    ERS_LOG(sout << "m_TriggerWordAddress    = " << m_TriggerWordAddress);
    ERS_LOG(sout << "m_TriggerWordSubaddress = " << m_TriggerWordSubaddress );

    ERS_LOG(sout << "m_CyclesForConfigure = ");
    this->dumpCycles(m_CyclesForConfigure);
    ERS_LOG(sout << "m_CyclesForConnect = ");
    this->dumpCycles(m_CyclesForConnect);
    ERS_LOG(sout << "m_CyclesForPrepareForRun = ");
    this->dumpCycles(m_CyclesForPrepareForRun);

    ERS_LOG(sout << ">>> BGo Summary <<<");
    // final summary of each of the four channels
    for( int i = 0; i < TTCVI::BGO_NUM; i++) {
      ERS_LOG(sout << "BGo Mode[" << i << "] = 0x" << std::hex << m_Mode[i] );
      ERS_LOG(sout << "BGo Delay[" << i << "] = 0x" << std::hex << m_InhibitDelay[i] );
      ERS_LOG(sout << "BGo Duration[" << i << "] = 0x" << std::hex << m_InhibitDuration[i] );
      ERS_LOG(sout << "BGo Retransmit[" << i << "] = 0x" << std::hex << m_Retransmit[i] );
      ERS_LOG(sout << "BGo CycleList[" << i << "] = ");
      this->dumpCycles(m_CycleList[i]);
    }
  }
  ERS_LOG(sout << "Done");
}

//-----------------------------------------------------------------------------
void TTCviModule::setup_TTCviConfig(const TTCvidal::TTCviConfig* ttcviconf) {
  std::string sout = m_UID + "@TTCviModule::setup_TTCviConfig() ";
  ERS_LOG(sout << "Entered");

  // Orbit Input Selection
  string d = ttcviconf->get_OrbitInput();
  if(d == TTCvidal::TTCviConfig::OrbitInput::Internal)      m_OrbitInput = TTCVI::ORB_INT;
  else if(d == TTCvidal::TTCviConfig::OrbitInput::External) m_OrbitInput = TTCVI::ORB_EXT;
  else {
    ostringstream text;
    text << "Unknown Orbit input \"" << d << "\"; Using default \"External\"";
    m_OrbitInput = TTCVI::ORB_EXT;
    ERS_REPORT_IMPL( ers::warning, ers::Message, text.str(), );
  }
  ERS_LOG(sout << "m_OrbitInput =" << m_OrbitInput );

  // Event/Orbit counter selection
  d = ttcviconf->get_CounterSelection();
  if(d == TTCvidal::TTCviConfig::CounterSelection::Orbit)      m_CounterSelection = TTCVI::CNT_ORB;
  else if(d ==  TTCvidal::TTCviConfig::CounterSelection::L1A) m_CounterSelection = TTCVI::CNT_L1A;
  else {
    ostringstream text;
    text << "Unknown Counter \"" << d << "\"; Using default \"L1A\"";
    ERS_REPORT_IMPL( ers::warning, ers::Message, text.str(), );
    m_CounterSelection = TTCVI::CNT_L1A;
  }
  ERS_LOG(sout << "m_CounterSelection = " << m_CounterSelection );

  // L1A input selection
  d = ttcviconf->get_L1AInput();
  if(d ==  TTCvidal::TTCviConfig::L1AInput::Ext0)      m_L1AInput = TTCVI::L1A_EXT0;
  else if(d == TTCvidal::TTCviConfig::L1AInput::Ext1) m_L1AInput = TTCVI::L1A_EXT1;
  else if(d == TTCvidal::TTCviConfig::L1AInput::Ext2) m_L1AInput = TTCVI::L1A_EXT2;
  else if(d == TTCvidal::TTCviConfig::L1AInput::Ext3) m_L1AInput = TTCVI::L1A_EXT3;
  else if(d == TTCvidal::TTCviConfig::L1AInput::Vme)  m_L1AInput = TTCVI::L1A_VME;
  else if(d == TTCvidal::TTCviConfig::L1AInput::Rndm) m_L1AInput = TTCVI::L1A_RNDM;
  else if(d == TTCvidal::TTCviConfig::L1AInput::Calib)  m_L1AInput = TTCVI::L1A_CALIB;
  else if(d == TTCvidal::TTCviConfig::L1AInput::Disabled)  m_L1AInput = TTCVI::L1A_DISABLE;
  else {
    ostringstream text;
    text << "Unknown L1A input \"" << d << "\"; Using default \"ext0\"";
    ERS_REPORT_IMPL( ers::warning, ers::Message, text.str(), );
    m_L1AInput = TTCVI::L1A_EXT0;
  }
  ERS_LOG(sout << "m_L1AInput = " << m_L1AInput );

  // L1A random generator frequency
  d = ttcviconf->get_L1ARandomGeneratorFrequency();
  if(d == TTCvidal::TTCviConfig::L1ARandomGeneratorFrequency::_1Hz)         m_L1ARandomGeneratorFrequency = TTCVI::RNDM_1HZ;
  else if(d ==  TTCvidal::TTCviConfig::L1ARandomGeneratorFrequency::_100Hz)  m_L1ARandomGeneratorFrequency = TTCVI::RNDM_100HZ;
  else if(d ==  TTCvidal::TTCviConfig::L1ARandomGeneratorFrequency::_1kHz)   m_L1ARandomGeneratorFrequency = TTCVI::RNDM_1KHZ;
  else if(d ==  TTCvidal::TTCviConfig::L1ARandomGeneratorFrequency::_5kHz)   m_L1ARandomGeneratorFrequency = TTCVI::RNDM_5KHZ;
  else if(d ==  TTCvidal::TTCviConfig::L1ARandomGeneratorFrequency::_10kHz)  m_L1ARandomGeneratorFrequency = TTCVI::RNDM_10KHZ;
  else if(d ==  TTCvidal::TTCviConfig::L1ARandomGeneratorFrequency::_25kHz)  m_L1ARandomGeneratorFrequency = TTCVI::RNDM_25KHZ;
  else if(d ==  TTCvidal::TTCviConfig::L1ARandomGeneratorFrequency::_50kHz)  m_L1ARandomGeneratorFrequency = TTCVI::RNDM_50KHZ;
  else if(d ==  TTCvidal::TTCviConfig::L1ARandomGeneratorFrequency::_100kHz) m_L1ARandomGeneratorFrequency = TTCVI::RNDM_100KHZ;
  else {
    ostringstream text;
    text << "Unknown L1A random generator frequency \"" << d << "]; Using default \"1Hz\"";
    ERS_REPORT_IMPL( ers::warning, ers::Message, text.str(), );
    m_L1ARandomGeneratorFrequency = TTCVI::RNDM_1HZ;
  }
  ERS_LOG(sout << "m_L1ARandomGeneratorFrequency = " << m_L1ARandomGeneratorFrequency );

  // Trigger word
  m_TriggerWordEnable = ttcviconf->get_TriggerWordEnable();
  m_TriggerWordExternal = ttcviconf->get_TriggerWordExternal();
  m_TriggerWordAddress = ttcviconf->get_TriggerWordAddress();
  m_TriggerWordSubaddress = ttcviconf->get_TriggerWordSubaddress();
  ERS_LOG(sout << "m_TriggerWordEnable     = " << m_TriggerWordEnable);
  ERS_LOG(sout << "m_TriggerWordExternal   = " << m_TriggerWordExternal);
  ERS_LOG(sout << "m_TriggerWordAddress    = " << m_TriggerWordAddress);
  ERS_LOG(sout << "m_TriggerWordSubaddress = " << m_TriggerWordSubaddress );

  // Cycles for transitions
  m_CyclesForConfigure = this->transform(ttcviconf->get_CyclesForConfigure());
  m_CyclesForConnect = this->transform(ttcviconf->get_CyclesForConnect());
  m_CyclesForPrepareForRun = this->transform(ttcviconf->get_CyclesForPrepareForRun());
  
  // reset BGoChannel Configuration
  this->resetBGoChannelConfiguration();

  // go through the list of BGo channels and overwrite the corresponding channels
  std::vector<const TTCvidal::TTCviBGoChannelBase*>::const_iterator it;
  for (it = ttcviconf->get_BGoChannels().begin(); it != ttcviconf->get_BGoChannels().end(); ++it) {
    if (const TTCvidal::TTCviBCR* bcr = m_confDB->cast<TTCvidal::TTCviBCR>(*it)) {
      ERS_LOG(sout << "Found BCR channel \"" << bcr->UID() << "\"");
      this->setup_BCR(bcr);
    } else if (const TTCvidal::TTCviECR* ecr = m_confDB->cast<TTCvidal::TTCviECR>(*it)) {
      ERS_LOG(sout << "Found ECR channel \"" << ecr->UID() << "\"");
      this->setup_ECR();
    } else if (const TTCvidal::TTCviBGoChannel* bchan = m_confDB->cast<TTCvidal::TTCviBGoChannel>(*it)) {

      // Read configuration parameters of the current B-channel
      ERS_LOG(sout << "Found BGo channel \"" << bchan->UID() << "\"");
      this->setup_BGoChannel(bchan);

    }
  }

  ERS_LOG(sout << "Done");
}

//-----------------------------------------------------------------------------
void TTCviModule::setup_TTCviSimpleConfig(const TTCvidal::TTCviSimpleConfig* ttcviconf) {
  std::string sout = m_UID + "@TTCviModule::setup_TTCviSimpleConfig() ";
  ERS_LOG(sout << "Entered");

  m_OrbitInput = TTCVI::ORB_EXT;
  ERS_LOG(sout << "m_OrbitInput =" << m_OrbitInput );
  m_CounterSelection = TTCVI::CNT_L1A;
  ERS_LOG(sout << "m_CounterSelection = " << m_CounterSelection );
  m_L1ARandomGeneratorFrequency = TTCVI::RNDM_1HZ;
  ERS_LOG(sout << "m_L1ARandomGeneratorFrequency = " << m_L1ARandomGeneratorFrequency );

  // L1A input selection
  string d = ttcviconf->get_Trigger();
  if(d == TTCvidal::TTCviSimpleConfig::Trigger::L1A)      m_L1AInput = TTCVI::L1A_EXT0;
  else if(d == TTCvidal::TTCviSimpleConfig::Trigger::TestTrigger1) m_L1AInput = TTCVI::L1A_EXT1;
  else if(d == TTCvidal::TTCviSimpleConfig::Trigger::TestTrigger2) m_L1AInput = TTCVI::L1A_EXT2;
  else if(d == TTCvidal::TTCviSimpleConfig::Trigger::TestTrigger3) m_L1AInput = TTCVI::L1A_EXT3;
  else {
    ostringstream text;
    text << "Unknown Trigger \"" << d << "\"; Using default \"ext0\"";
    ERS_LOG(text.str());
    m_L1AInput = TTCVI::L1A_EXT0;
  }
  ERS_LOG(sout << "m_L1AInput = " << m_L1AInput );

  d = ttcviconf->get_TriggerWord();
  if (d == TTCvidal::TTCviSimpleConfig::TriggerWord::Do_not_use) {
    m_TriggerWordEnable = false;
    m_TriggerWordExternal = false;
    m_TriggerWordAddress = 0;
    m_TriggerWordSubaddress = 0;
  }
  else if (d == TTCvidal::TTCviSimpleConfig::TriggerWord::Use_internal) {
    m_TriggerWordEnable = true;
    m_TriggerWordExternal = false;
    m_TriggerWordAddress = 0;
    m_TriggerWordSubaddress = 0;
  }
  else if (d == TTCvidal::TTCviSimpleConfig::TriggerWord::Use_external) {
    m_TriggerWordEnable = true;
    m_TriggerWordExternal = true;
    m_TriggerWordAddress = 0;
    m_TriggerWordSubaddress = 0;
  }
  else {
    ostringstream text;
    text << "Unknown TriggerWord \"" << d << "\"; Using default \"do_not_use\"";
    ERS_REPORT_IMPL( ers::warning, ers::Message, text.str(), );
    m_TriggerWordEnable = false;
    m_TriggerWordExternal = false;
    m_TriggerWordAddress = 0;
    m_TriggerWordSubaddress = 0;
  }

  ERS_LOG(sout << "m_TriggerWordEnable     = " << m_TriggerWordEnable);
  ERS_LOG(sout << "m_TriggerWordExternal   = " << m_TriggerWordExternal);
  ERS_LOG(sout << "m_TriggerWordAddress    = " << m_TriggerWordAddress);
  ERS_LOG(sout << "m_TriggerWordSubaddress = " << m_TriggerWordSubaddress );

  // BCR
  u_int bcrdelay = ttcviconf->get_BCR_delay();
  this->setup_BCR(bcrdelay);

  // ECR
  this->setup_ECR();


  ERS_LOG(sout << "Done");
}


//-----------------------------------------------------------------------------
void TTCviModule::setup_BGoChannel(const TTCvidal::TTCviBGoChannel* bchan) {
  std::string sout = m_UID + "@TTCviModule::setup_BGoChannel() ";
  ERS_LOG(sout << "Entered");
  ERS_LOG(sout << "Setting up BChannel " << bchan->UID());

  u_int channel = bchan->get_ChannelNumber();
  ERS_LOG(sout << "> ChannelNumber = " << channel);
	
  m_Mode[channel] = 0;

  std::string d = bchan->get_Selection();
  ERS_LOG(sout << "> Selection = " << d);
  if (d == TTCvidal::TTCviBGoChannel::Selection::External) {
    m_Mode[channel] |= TTCVI::BGO_ENABLE;
  } else if ( d != TTCvidal::TTCviBGoChannel::Selection::VMEbus) {
    ostringstream text;
    text << "Unknown B-Channel Selection \"" << d << "\" for channel " << channel << "; Using default \"VMEbus\"";
    ERS_REPORT_IMPL( ers::error, ers::Message, text.str(), );
  }

  d = bchan->get_Cycle();
  ERS_LOG(sout << "> Cycle = " << d);
  if (d == TTCvidal::TTCviBGoChannel::Cycle::Synchronous) {
    m_Mode[channel] |= TTCVI::BGO_SYNC;
  } else if ( d != TTCvidal::TTCviBGoChannel::Cycle::Asynchronous) {
    ostringstream text;
    text << "Unknown B-Channel Cycle \"" << d << "\" for channel " << channel << "; Using default \"Asynchronous\"";
    ERS_REPORT_IMPL( ers::warning, ers::Message, text.str(), );
  }

  d = bchan->get_Mode();
  ERS_LOG(sout << "> Mode = " << d);
  if (d == TTCvidal::TTCviBGoChannel::Mode::Single) {
    m_Mode[channel] |= TTCVI::BGO_SINGLE;
  } else if ( d != TTCvidal::TTCviBGoChannel::Mode::Repetitive) {
    ostringstream text;
    text << "Unknown B-Channel Mode \"" << d << "\" for channel " << channel << "; Using default \"Repetitive\"";
    ERS_REPORT_IMPL( ers::warning, ers::Message, text.str(), );
  }

  d = bchan->get_CycleStart();
  ERS_LOG(sout << "> CycleStart = " << d);
  if (d == TTCvidal::TTCviBGoChannel::CycleStart::LookAtFifoStatus) {
    m_Mode[channel] |= TTCVI::BGO_FIFO;
  } else if ( d != TTCvidal::TTCviBGoChannel::CycleStart::DoNotLookAtFifoStatus) {
    ostringstream text;
    text << "Unknown B-Channel Mode \"" << d << "\" for channel " << channel << "; Using default \"DoNotLookAtFifoStatus\"";
    ERS_REPORT_IMPL( ers::warning, ers::Message, text.str(), );
  }

  d = bchan->get_CalibrationMode();
  ERS_LOG(sout << "> CalibrationMode = " << d);
  if (channel == 2) {
    if (d == TTCvidal::TTCviBGoChannel::CalibrationMode::Enable) {
      m_Mode[channel] |= TTCVI::BGO_CALIB;
    } else if ( d != TTCvidal::TTCviBGoChannel::CalibrationMode::Disable) {
      ostringstream text;
      text << "Unknown B-Channel CalibrationMode \"" << d << "\" for channel " << channel << "; Using default \"Disable\"";
      ERS_REPORT_IMPL( ers::warning, ers::Message, text.str(), );
    }
  }

  // Retransmit(bool)
  d = bchan->get_Retransmission();
  ERS_LOG(sout << "> Retransmission = " << d);
  if(d == TTCvidal::TTCviBGoChannel::Retransmission::Retransmit) {
    m_Retransmit[channel] = true;
  } else if (d == TTCvidal::TTCviBGoChannel::Retransmission::Stop) {
    m_Retransmit[channel] = false;
  } else {
    ostringstream text;
    text << "Unknown B-Channel Retransmission \"" << d << "\" for channel " << channel << "; Using default \"Retransmit\"";
    ERS_REPORT_IMPL( ers::warning, ers::Message, text.str(), );
    m_Retransmit[channel] = true;
  }
  // Inhibit Delay
  m_InhibitDelay[channel] = bchan->get_InhibitDelay();
  ERS_LOG(sout << "> InhibitDelay = 0x" << hex << m_InhibitDelay[channel] << " (" << dec << m_InhibitDelay[channel] << " dec)");
  // Inhibit Duration
  m_InhibitDuration[channel] = bchan->get_InhibitDuration();
  ERS_LOG(sout << "> InhibitDuration = " << hex << m_InhibitDuration[channel] << "(" << dec << m_InhibitDuration[channel] << " dec)");

  // Cycle list
  m_CycleList[channel] = this->transform(bchan->get_Cycles());

  this->dumpCycles(m_CycleList[channel]);

  ERS_LOG(sout << "Done");
}

//--------
void TTCviModule::dumpCycles(std::vector<u_int>& cycleList) {
  std::ostringstream helper;
  helper.clear();
  int i(0);
  helper << "Cycles = " << std::hex;
  for (std::vector<u_int>::const_iterator it = cycleList.begin();
       it != cycleList.end();
       ++it) {
    helper << "word(" << i << ")=0x" << *it << " ";
    ++i;
  }
  helper << std::dec;
  ERS_LOG(helper.str());
}
//--------

void TTCviModule::setup_BCR(const TTCvidal::TTCviBCR* bcr) {
  std::string sout = m_UID + "@TTCviModule::setup_BCR() ";
  ERS_LOG(sout << "Entered");
  ERS_LOG(sout << "Setting up BCR " << bcr->UID());
  this->setup_BCR(static_cast<unsigned int>(bcr->get_Delay()));
 
  ERS_LOG(sout << "Done");
}
//--------

void TTCviModule::setup_BCR(unsigned int delay) {
  std::string sout = m_UID + "@TTCviModule::setup_BCR() ";
  ERS_LOG(sout << "Entered");
  m_InhibitDelay[0] = delay;
  ERS_LOG(sout << "> Inhibit Delay = " << m_InhibitDelay[0]);
  m_InhibitDuration[0] = 51;
  m_Mode[0] = TTCVI::BGO_SYNC;
  m_Retransmit[0] = true;
  m_CycleList[0].clear();
  u_int bcr = (0x1 << 23);
  m_CycleList[0].push_back(bcr);
  this->dumpCycles(m_CycleList[0]);

  ERS_LOG(sout << "Done");
}
//--------

void TTCviModule::setup_ECR() {
  std::string sout = m_UID + "@TTCviModule::setup_ECR() ";
  ERS_LOG(sout << "Entered");

  m_InhibitDelay[1] = 0;
  m_InhibitDuration[1] = 1;
  m_Mode[1] = TTCVI::BGO_ENABLE+TTCVI::BGO_SINGLE;
  m_Retransmit[1] = true;
  m_CycleList[1].clear();
  u_int ecr = (0x2 << 23);
  m_CycleList[1].push_back(ecr);
  this->dumpCycles(m_CycleList[1]);

  ERS_LOG(sout << "Done");
}
//--------

DFCountedPointer<Config> TTCviModule::getInfo()
{
  std::string sout = m_UID + "@TTCviModule::getInfo() ";
  ERS_DEBUG(1, sout << "Entered");

  DFCountedPointer<Config> configuration = Config::New();
  
  ERS_DEBUG(1, sout << "Done");
  return configuration;
}

void TTCviModule::clearInfo()
{
  std::string sout = m_UID + "@TTCviModule::clearInfo() ";
  ERS_LOG(sout << "Entered");
  ERS_LOG(sout << "Done");
}
  
void TTCviModule::configure(const daq::rc::TransitionCmd&) {
  std::string sout = m_UID + "@TTCviModule::configure() ";
  ERS_LOG(sout << "Entered");

  // Create TTCvi
  m_ttcvi = new TTCVI(m_PhysAddress);
  
  ERS_LOG(sout << "Creating TTCvi at 0x" << std::hex << m_PhysAddress << std::dec);
  // Checking TTCvi
  if((m_status = (*m_ttcvi)()) == 0) {
    ERS_LOG(sout << "TTCvi created at 0x" << std::hex << m_PhysAddress << std::dec);
  }
  else {    
    ERS_LOG(sout << "TTCvi not created");
    ostringstream text;
    text << "TTCvi not properly created: status = " << m_status ;
    ERS_REPORT_IMPL( ers::error, ers::Message, text.str(), );
  }


  if (m_useConfiguration) {

    // Trigger mode is "DISABLE" in initial state
    if((m_status = m_ttcvi->l1aInputSet(TTCVI::L1A_DISABLE))!=0) {
      ostringstream text;
      text << "Could not disable TTCvi trigger mode; status = " << m_status ;
      ERS_REPORT_IMPL( ers::warning, ers::Message, text.str(), );
    }

    // Confirm BGO retransmit bit is reset.
    ERS_LOG(sout << "Set BGo retransmit flags");
    for(int i=0; i< TTCVI::BGO_NUM; i++) {
      if((m_status = m_ttcvi->bgoFifoRetransSet(i, false))!=0) {
	ostringstream text;
	text << "Could not reset B-GO<" << i << ">  retransmit bit ; status = " << m_status ;
	ERS_REPORT_IMPL( ers::warning, ers::Message, text.str(), );
      }
    }

    // Reset TTCvi with clearing FIFO
    ERS_LOG(sout << "Resetting registers of TTCvi");
    if((m_status = m_ttcvi->reset(true))!=0) {  
      ostringstream text;
      text << "Could not reset TTCvi; status = " << m_status ;
      ERS_REPORT_IMPL( ers::warning, ers::Message, text.str(), );
    }

    // Get manufacturer ID
    if((m_status = m_ttcvi->manufacturerGet(&m_manufacturerID))!=0) {
      ostringstream text;
      text << "Could not get manufacturer ID; status = " << m_status ;
      ERS_REPORT_IMPL( ers::warning, ers::Message, text.str(), );
    }
    ERS_LOG(sout << "manufacturer ID" << m_manufacturerID << std::hex << " (0x" << m_manufacturerID << ")" << std::dec);

    // Get board ID
    if((m_status = m_ttcvi->boardIdentifierGet(&m_boardID))!=0) {
      ostringstream text;
      text << "Could not board ID; status = " << m_status ;
      ERS_REPORT_IMPL( ers::warning, ers::Message, text.str(), );
    }
    m_serialNo  = m_boardID & 0xFFFF;
    ERS_LOG(sout << "Serial No. = " << m_serialNo << " (0x" << std::hex << m_serialNo << ")" << std::dec);

    // B-GO FIFO should be reset by bgoFifoReset method if the manufacturer
    // is not 0xEFACEC or the serial no. is less than equal 80.
    if( (m_manufacturerID != 0x080030 ) || (m_serialNo <= 80) ) {
      // Reset B-GO<i> FIFOs
      for(int i=0; i< 3; i++)
	if((m_status = m_ttcvi->bgoFifoReset(i))!=0) {
	  ostringstream text;
	  text << "Could not reset TTCvi B-GO<" << i << "> FIFOs; status = " << m_status ;
	  ERS_REPORT_IMPL( ers::warning, ers::Message, text.str(), );
	}
    }

    // Disable Trigger Word
    if((m_status = m_ttcvi->triggerWordDisable())!=0) {
      ostringstream text;
      text << "Could not disable TTCvi trigger word; status = " << m_status ;
      ERS_REPORT_IMPL( ers::warning, ers::Message, text.str(), );
    }


    //
    // Setup trigger input
    //
    ERS_LOG(sout << "Setting L1A input");
    if ((m_status = m_ttcvi->l1aInputSet(static_cast<u_short>(m_L1AInput))) !=0) {
      ostringstream text;
      text << "TTCvi::l1aInputSet returned with status = " << m_status ;
      ERS_REPORT_IMPL( ers::warning, ers::Message, text.str(), );
    }

    // Set the Orbit input: is it internal or external (that is from fron panel)?
    ERS_LOG(sout << "Setting Orbit input");
    if ((m_status = m_ttcvi->orbitInputSet(static_cast<u_short>(m_OrbitInput))) !=0) {
      ostringstream text;
      text << "TTCvi::orbitInputSet returned with status = " << m_status ;
      ERS_REPORT_IMPL( ers::warning, ers::Message, text.str(), );
    }
  
    // If L1A input selection is "random", set the L1A random generator frequency
    if ((m_L1AInput == TTCVI::L1A_RNDM)) {
      ERS_LOG(sout << "TTCvi:: set L1A random generator frequency");
      if ((m_status = m_ttcvi->l1aRandomSet(static_cast<u_short>(m_L1ARandomGeneratorFrequency))) !=0) {
	ostringstream text;
	text << "TTCvi::l1aRandomSet returned with status = " << m_status ;
	ERS_REPORT_IMPL( ers::warning, ers::Message, text.str(), );
      }
    }
      
    // Set the Counter selection (is it counting events or bunch crossings)
    ERS_LOG(sout << "Setting Counter selection");
    if ((m_status = m_ttcvi->counterSelectionSet(static_cast<u_short>(m_CounterSelection))) !=0) {
      ostringstream text;
      text << "TTCVI::counterSelectionSet returned with status = " << m_status ;
      ERS_REPORT_IMPL( ers::warning, ers::Message, text.str(), );
    }

    // Set the B-channels modes
    ERS_LOG(sout << "Setting BGo channel modes");
    for (int i = 0; i < TTCVI::BGO_NUM; i++) {
      ERS_LOG(sout << "set mode of B-channel" << i);
      if ((m_status = m_ttcvi->bgoModeSet(i, static_cast<u_short>(m_Mode[i]))) != 0) {
	ostringstream text;
	text << "bgoModeSet returned with status = " << m_status;
	ERS_REPORT_IMPL( ers::warning, ers::Message, text.str(), );
      }
    }

    // Set the retransmit flag of B channels
    ERS_LOG(sout << "Setting BGo channel retransmit flags");
    for (int i = 0; i < TTCVI::BGO_NUM; i++) {
      if((m_status = m_ttcvi->bgoFifoRetransSet(i, m_Retransmit[i]))!=0) {
	ostringstream text;
	text << "Could not reset B-GO<" << i << ">  retransmit bit ; status = " << m_status ;
	ERS_REPORT_IMPL( ers::warning, ers::Message, text.str(), );

      }
    }

    // Set the B-channels cycle
    ERS_LOG(sout << "Setting BGo FIFOs");
    for (int i = 0; i < TTCVI::BGO_NUM; i++) {
      for (std::vector<u_int>::const_iterator it = m_CycleList[i].begin();
	   it != m_CycleList[i].end();
	   ++it) {

	ERS_LOG("Putting cycle into BGo-Fifo[" << i << "] = 0x"
		<< std::hex << *it << std::dec);
	if ((m_status = m_ttcvi->bgoCommandPut(i, *it)) != 0) {
	  ostringstream text;
	  text << "TTCVI::bgoCommandPut returned with status = " << m_status ;
	  ERS_REPORT_IMPL( ers::warning, ers::Message, text.str(), );
	}

      }
    }

    // Enable trigger word if required
    if (m_TriggerWordEnable) {
      ERS_LOG(sout << "Enable trigger word");
      m_lcmd.address = m_TriggerWordAddress;
      m_lcmd.external = m_TriggerWordExternal;
      m_lcmd.sub_address = m_TriggerWordSubaddress;
      if ((m_status = m_ttcvi->triggerWordEnable(m_lcmd)) !=0) {
	ostringstream text;
	text << "TTCvi::triggerWordEnable returned with status = " << m_status ;
	ERS_REPORT_IMPL( ers::warning, ers::Message, text.str(), );
      }
    }

    // send cycles
    this->sendCycles(m_CyclesForConfigure);
  }

  // publish to IS
  this->publish();

  ERS_LOG(sout << "Done");
}
	
//------------------------------------------------------------------------------
void TTCviModule::connect(const daq::rc::TransitionCmd&) {
  std::string sout = m_UID + "@TTCviModule::connect() ";
  ERS_LOG(sout << "Entered");

  if (m_useConfiguration) {
    // Switch on inhibit signal associated to B-channels
    for (int i = 0; i < TTCVI::BGO_NUM; i++) {
      ERS_LOG(sout << "TTCviModule::connect: start inhibit for B-channel " << i);
      if ((m_status = m_ttcvi->bgoInhibitOn(i, static_cast<u_short>(m_InhibitDelay[i]), static_cast<u_short>(m_InhibitDuration[i]))) != 0) {
	ostringstream text;
	text << "TTCvi::bgoInhibitOn returned with status = " << m_status ;
	ERS_REPORT_IMPL( ers::warning, ers::Message, text.str(), );
      }
    }

    // Printout registers
    ERS_LOG(sout << "Printing out registers of TTCvi");
    if((m_status = m_ttcvi->dump()) != 0) {  
      ostringstream text;
      text << "Problem printing out registers of TTCvi: status = " << m_status ;
      ERS_REPORT_IMPL( ers::warning, ers::Message, text.str(), );
    }

    this->sendCycles(m_CyclesForConnect);
  }

  ERS_LOG(sout << "Done");
}

void TTCviModule::prepareForRun(const daq::rc::TransitionCmd&) {
  std::string sout = m_UID + "@TTCviModule::prepareForRun() ";
  ERS_LOG(sout << "Entered");

  // dump the registers... 
  ERS_LOG(sout << "TTCviModule::PrepareForRun -- dump registers");
  if((m_status = m_ttcvi->dump()) != 0) {
    ostringstream text;
    text << "Could not print out registers of TTCvi; status = " << m_status ;
    ERS_REPORT_IMPL( ers::warning, ers::Message, text.str(), );
  }

  if (m_useConfiguration) {
    this->sendCycles(m_CyclesForPrepareForRun);
  }

  if (m_useMonitoring || m_useConfiguration) {
    // Reset the counter
    ERS_LOG(sout << "TTCviModule::prepareForRun: Reset counter");
    if ((m_status = m_ttcvi->counterReset()) !=0) {
      ostringstream text;
      text << "TTCvi::counterReset returned with status = " << m_status ;
      ERS_REPORT_IMPL( ers::warning, ers::Message, text.str(), );
    }
  }

  ERS_LOG(sout << "Done");
}


void TTCviModule::stopDC(const daq::rc::TransitionCmd&) {
  std::string sout = m_UID + "@TTCviModule::stopEB() ";
  ERS_LOG(sout << "Entered");
  
  ERS_LOG(sout << "Done");
}

//------------------------------------------------------------------------------
void TTCviModule::disconnect(const daq::rc::TransitionCmd&) {
  std::string sout = m_UID + "@TTCviModule::disconnect() ";
  ERS_LOG(sout << "Entered");

  if (m_useConfiguration) {
    // Inhibit off the B-GO channels
    for (int i = 0; i < TTCVI::BGO_NUM; i++) {
      ERS_LOG(sout << "TTCviModule:disconnect: inhibit off for B-channel " << i << " to 0");
      if ((m_status = m_ttcvi->bgoInhibitOff(i)) != 0) {
	ostringstream text;
	text << "TTCvi::bgoInhibitOff returned with status = " << m_status ;
	ERS_REPORT_IMPL( ers::warning, ers::Message, text.str(), );
      }
    }

  }
  ERS_LOG(sout << "Done");
}

//------------------------------------------------------------------------------
void TTCviModule::unconfigure(const daq::rc::TransitionCmd&) {
  std::string sout = m_UID + "@TTCviModule::unconfigure() ";
  ERS_LOG(sout << "Entered");

  if (m_useConfiguration) {
    // ignore trigger operation except VME, Random or Calibration trigger

    // Inhibit off the B-channels
    for (int i = 0; i < TTCVI::BGO_NUM; i++) {
      ERS_LOG(sout << "TTCviModule::unconfigure: inhibit off for B-channel " << i << " to 0");
      if ((m_status = m_ttcvi->bgoInhibitOff(i)) != 0) {
	ostringstream text;
	text << "TTCvi::bgoInhibitOff returned with status = " << m_status ;
	ERS_REPORT_IMPL( ers::warning, ers::Message, text.str(), );
      }
    }

    ERS_LOG(sout << "TTCviModule::unconfigure: set L1A input to disabled");
    if ((m_status = m_ttcvi->l1aInputSet(TTCVI::L1A_DISABLE)) !=0) {
      ostringstream text;
      text << "TTCvi::l1aInputSet returned with status = " << m_status ;
      ERS_REPORT_IMPL( ers::warning, ers::Message, text.str(), );
    }
    
    // Disable trigger word
    ERS_LOG(sout << "TTCviModule::unconfigure: Disable trigger word");
    if ((m_status = m_ttcvi->triggerWordDisable()) !=0) {
      ostringstream text;
      text << "TTCvi::triggerWordDisable returned with status = " << m_status ;
      ERS_REPORT_IMPL( ers::warning, ers::Message, text.str(), );
    }
  }

  if (m_ttcvi) {
    delete m_ttcvi;
    m_ttcvi=nullptr;
  }

  ERS_LOG(sout << "Done");
}


/******************************************************/
void TTCviModule::publish()
/******************************************************/
{
  std::string sout = m_UID + "@TTCviModule::publish() ";
  ERS_DEBUG(1, sout << "Entered");

  if (m_useMonitoring) {
    const std::string dict_entry = m_IS_server + "." + m_UID + "/TTCVI";
    ERS_DEBUG(1, sout << dict_entry.c_str());

    TTCVI_ISNamed is_entry(m_ipcpartition, dict_entry);

    int countervalue;
    m_ttcvi->counterValueGet(&countervalue);
    is_entry.CounterValue  = static_cast<unsigned long>(countervalue);

    u_short sel;
    m_ttcvi->counterSelectionGet(&sel);
    switch (sel) {
    case RCD::TTCVI::CNT_L1A:
      is_entry.CounterName = "L1A";
      break;
    case RCD::TTCVI::CNT_ORB:
      is_entry.CounterName = "ORBIT";
      break;
    default:
      is_entry.CounterName = "Invalid";
      break;
    }

    int del;
    m_ttcvi->bcDelayGet(&del);
    is_entry.BCdelay = del;
    
    m_ttcvi->l1aInputGet(&sel);
    switch(sel) {
    case RCD::TTCVI::L1A_EXT0:
      is_entry.L1Aselection = "L1A";
      break;
    case RCD::TTCVI::L1A_EXT1:
      is_entry.L1Aselection = "TestTrigger-1";
      break;
    case RCD::TTCVI::L1A_EXT2:
      is_entry.L1Aselection = "TestTrigger-2";
      break;
    case RCD::TTCVI::L1A_EXT3:
      is_entry.L1Aselection = "TestTrigger-3";
      break;
    case RCD::TTCVI::L1A_VME:
      is_entry.L1Aselection = "VME";
      break;
    case RCD::TTCVI::L1A_RNDM:
      is_entry.L1Aselection = "Random";
      break;
    case RCD::TTCVI::L1A_CALIB:
      is_entry.L1Aselection = "Calibration";
      break;
    case RCD::TTCVI::L1A_DISABLE:
      is_entry.L1Aselection = "Disable";
      break;
    default:
      is_entry.L1Aselection = "Invalid";
      break;
    }

    m_ttcvi->orbitInputGet(&sel);
    switch(sel) {
    case RCD::TTCVI::ORB_INT:
      is_entry.ORBITselection = "Internal";
      break;
    case RCD::TTCVI::ORB_EXT:
      is_entry.ORBITselection = "External";
      break;
    default:
      is_entry.ORBITselection = "Invalid";
      break;
    }

    m_ttcvi->l1aRandomGet(&sel);
    switch(sel) {
    case RCD::TTCVI::RNDM_1HZ:
      is_entry.RandomTriggerRate = "1Hz";
      break;
    case RCD::TTCVI::RNDM_100HZ:
      is_entry.RandomTriggerRate = "100Hz";
      break;
    case RCD::TTCVI::RNDM_1KHZ:
      is_entry.RandomTriggerRate = "1kHz";
      break;
    case RCD::TTCVI::RNDM_5KHZ:
      is_entry.RandomTriggerRate = "5kHz";
      break;
    case RCD::TTCVI::RNDM_10KHZ:
      is_entry.RandomTriggerRate = "10kHz";
      break;
    case RCD::TTCVI::RNDM_25KHZ:
      is_entry.RandomTriggerRate = "25kHz";
      break;
    case RCD::TTCVI::RNDM_50KHZ:
      is_entry.RandomTriggerRate = "50kHz";
      break;
    case RCD::TTCVI::RNDM_100KHZ:
      is_entry.RandomTriggerRate = "100kHz";
      break;
    }
    
    bool isempty;
    m_ttcvi->l1aFifoEmpty(&isempty);
    if (isempty) is_entry.L1A_FIFO = "empty";
    else         is_entry.L1A_FIFO = "not empty";

    m_ttcvi->bgoModeGet(0, &sel);
    if (sel & RCD::TTCVI::BGO_ENABLE) is_entry.BGO0 += "enabled ";
    else                              is_entry.BGO0 += "disabled ";
    if (sel & RCD::TTCVI::BGO_SYNC)   is_entry.BGO0 += "synchronous ";
    else                              is_entry.BGO0 += "asynchronous ";
    if (sel & RCD::TTCVI::BGO_SINGLE) is_entry.BGO0 += "single ";
    else                              is_entry.BGO0 += "repetitive ";
    if (sel & RCD::TTCVI::BGO_FIFO)   is_entry.BGO0 += "fifo ";
    else                              is_entry.BGO0 += "no_fifo ";
    if (sel & RCD::TTCVI::BGO_CALIB)  is_entry.BGO0 += "basic ";
    else                              is_entry.BGO0 += "basic ";

    m_ttcvi->bgoModeGet(1, &sel);
    if (sel & RCD::TTCVI::BGO_ENABLE) is_entry.BGO1 += "enabled ";
    else                              is_entry.BGO1 += "disabled ";
    if (sel & RCD::TTCVI::BGO_SYNC)   is_entry.BGO1 += "synchronous ";
    else                              is_entry.BGO1 += "asynchronous ";
    if (sel & RCD::TTCVI::BGO_SINGLE) is_entry.BGO1 += "single ";
    else                              is_entry.BGO1 += "repetitive ";
    if (sel & RCD::TTCVI::BGO_FIFO)   is_entry.BGO1 += "fifo ";
    else                              is_entry.BGO1 += "no_fifo ";
    if (sel & RCD::TTCVI::BGO_CALIB)  is_entry.BGO1 += "basic ";
    else                              is_entry.BGO1 += "basic ";

    m_ttcvi->bgoModeGet(2, &sel);
    if (sel & RCD::TTCVI::BGO_ENABLE) is_entry.BGO2 += "enabled ";
    else                              is_entry.BGO2 += "disabled ";
    if (sel & RCD::TTCVI::BGO_SYNC)   is_entry.BGO2 += "synchronous ";
    else                              is_entry.BGO2 += "asynchronous ";
    if (sel & RCD::TTCVI::BGO_SINGLE) is_entry.BGO2 += "single ";
    else                              is_entry.BGO2 += "repetitive ";
    if (sel & RCD::TTCVI::BGO_FIFO)   is_entry.BGO2 += "fifo ";
    else                              is_entry.BGO2 += "no_fifo ";
    if (sel & RCD::TTCVI::BGO_CALIB)  is_entry.BGO2 += "calibration ";
    else                              is_entry.BGO2 += "basic ";

    m_ttcvi->bgoModeGet(3, &sel);
    if (sel & RCD::TTCVI::BGO_ENABLE) is_entry.BGO3 += "enabled ";
    else                              is_entry.BGO3 += "disabled ";
    if (sel & RCD::TTCVI::BGO_SYNC)   is_entry.BGO3 += "synchronous ";
    else                              is_entry.BGO3 += "asynchronous ";
    if (sel & RCD::TTCVI::BGO_SINGLE) is_entry.BGO3 += "single ";
    else                              is_entry.BGO3 += "repetitive ";
    if (sel & RCD::TTCVI::BGO_FIFO)   is_entry.BGO3 += "fifo ";
    else                              is_entry.BGO3 += "no_fifo ";
    if (sel & RCD::TTCVI::BGO_CALIB)  is_entry.BGO3 += "basic ";
    else                              is_entry.BGO3 += "basic ";

  

    u_short bgodel, bgodur;
    m_ttcvi->bgoInhibitGet(0, &bgodel, &bgodur);
    is_entry.BGO0_inhibit_del = bgodel;
    is_entry.BGO0_inhibit_dur = bgodur;
    m_ttcvi->bgoInhibitGet(1, &bgodel, &bgodur);
    is_entry.BGO1_inhibit_del = bgodel;
    is_entry.BGO1_inhibit_dur = bgodur;
    m_ttcvi->bgoInhibitGet(2, &bgodel, &bgodur);
    is_entry.BGO2_inhibit_del = bgodel;
    is_entry.BGO2_inhibit_dur = bgodur;
    m_ttcvi->bgoInhibitGet(3, &bgodel, &bgodur);
    is_entry.BGO3_inhibit_del = bgodel;
    is_entry.BGO3_inhibit_dur = bgodur;
    
    m_ttcvi->bgoFifoEmpty(0, &isempty);
    if (isempty) is_entry.BGO0_FIFO = "empty";
    else         is_entry.BGO0_FIFO = "not empty";

    m_ttcvi->bgoFifoEmpty(1, &isempty);
    if (isempty) is_entry.BGO1_FIFO = "empty";
    else         is_entry.BGO1_FIFO = "not empty";

    m_ttcvi->bgoFifoEmpty(2, &isempty);
    if (isempty) is_entry.BGO2_FIFO = "empty";
    else         is_entry.BGO2_FIFO = "not empty";

    m_ttcvi->bgoFifoEmpty(3, &isempty);
    if (isempty) is_entry.BGO3_FIFO = "empty";
    else         is_entry.BGO3_FIFO = "not empty";

    bool retransmit(0);
    m_ttcvi->bgoFifoRetransGet(0, &retransmit);
    if (retransmit) is_entry.BGO0_retransmit = "retransmit";
    else            is_entry.BGO0_retransmit = "stop";

    m_ttcvi->bgoFifoRetransGet(1, &retransmit);
    if (retransmit) is_entry.BGO1_retransmit = "retransmit";
    else            is_entry.BGO1_retransmit = "stop";

    m_ttcvi->bgoFifoRetransGet(2, &retransmit);
    if (retransmit) is_entry.BGO2_retransmit = "retransmit";
    else            is_entry.BGO2_retransmit = "stop";

    m_ttcvi->bgoFifoRetransGet(3, &retransmit);
    if (retransmit) is_entry.BGO3_retransmit = "retransmit";
    else            is_entry.BGO3_retransmit = "stop";
    
    // Trigger Word
    TTCVI::LongCommand lcmd;
    bool flag;
    m_ttcvi->triggerWordGet(&lcmd,&flag);
    is_entry.TriggerWord_address = lcmd.address;
    is_entry.TriggerWord_external = (lcmd.external==1)? true : false;
    is_entry.TriggerWord_subaddress = lcmd.sub_address;
    is_entry.TriggerWord_enable = flag;

    try {
      is_entry.checkin();
    } catch (daq::is::Exception & ex) {
      ERS_LOG(sout << "Could not publish to IS. Exception caught: " << ex.what());
    } catch (CORBA::SystemException& ex) {
      ERS_LOG(sout << "Could not publish to IS. Exception caught: " << ex._name());
    } catch (std::exception& ex) {
      ERS_LOG(sout << "Could not publish to IS. Exception caught: " << ex.what());
    } catch (...) {
      ERS_LOG(sout << "Could not publish to IS. Unknown exception caught and rethrown.");
      throw;
    }
  }
  ERS_DEBUG(1, sout << "Done");
}
 
/******************************************************/
void TTCviModule::publishFullStats()
/******************************************************/
{  
  std::string sout = m_UID + "@TTCviModule::publishFullStats() ";
  ERS_DEBUG(1, sout << "Entered");

  // publish counter value
  this->publish();

  ERS_DEBUG(1, sout << "Done");
}    


/******************************************************/
void TTCviModule::resetConfiguration() {
  /******************************************************/
  m_UID = "";
  m_PhysAddress = 0;
  m_OperationMode = "";
  m_OrbitInput = 0;
  m_CounterSelection = 0;
  m_L1AInput = 0;
  m_L1ARandomGeneratorFrequency = 0;
  m_TriggerWordEnable = false;
  m_TriggerWordExternal = false;
  m_TriggerWordAddress = 0;
  m_TriggerWordSubaddress = 0;
  m_IS_server = "";
  m_useConfiguration = false;
  m_useMonitoring = false;
  this->resetBGoChannelConfiguration();
  m_CyclesForConfigure.clear();
  m_CyclesForConnect.clear();
  m_CyclesForPrepareForRun.clear();
}

/******************************************************/
void  TTCviModule::resetBGoChannelConfiguration() {
  /******************************************************/
  m_Mode.clear();
  m_InhibitDelay.clear();
  m_InhibitDuration.clear();
  m_Retransmit.clear();
  m_CycleList.clear();
  // set to defaults
  for (int i = 0; i<TTCVI::BGO_NUM; ++i) {
    m_Mode.push_back(TTCVI::BGO_SINGLE); // disable output, async, single, no fifo, basic
    m_InhibitDelay.push_back(0);
    m_InhibitDuration.push_back(0);
    m_Retransmit.push_back(true);
    std::vector<u_int> list(0);
    m_CycleList.push_back(list);
  }
}


void TTCviModule::sendCycles(std::vector<u_int>& cycles) {

  for (std::vector<u_int>::const_iterator it = cycles.begin();
       it != cycles.end();
       ++it) {

    if ( ((*it) & (0x1<<31)) == 0x0 ) {
      TTCVI::ShortCommand scmd = static_cast<u_char>(((*it) & (0xff<<23))>>23);
      ERS_LOG("Sending asynchronous short-format cycle = 0x" 
	      << std::hex <<  static_cast<unsigned short>(scmd) << std::dec);
     
      if ((m_status = m_ttcvi->asyncCommand(scmd)) != 0) {
	ostringstream text;
	text << "TTCVI::asyncCommand returned with status = " << m_status ;
	ERS_REPORT_IMPL( ers::warning, ers::Message, text.str(), );
      }

    } else {

      TTCVI::LongCommand lcmd;
      lcmd.address = (((*it) & (0x3fff<<17)) >> 17);
      lcmd.external = (((*it)&(0x1<<16))!=0) ? true : false;
      lcmd.sub_address = (((*it) & (0xff<<8)) >>8);
      lcmd.data = (*it) & 0xff;

      ERS_LOG("Sending asynchronous long-format cycle: address=" 
	      << std::hex << lcmd.address 
	      << " ext=" << lcmd.external 
	      << " subaddr=0x" << static_cast<u_short>(lcmd.sub_address)
	      << " data=0x" << static_cast<u_short>(lcmd.data) << std::dec);

      if ((m_status = m_ttcvi->asyncCommand(lcmd)) != 0) {
	ostringstream text;
	text << "TTCVI::asyncCommand returned with status = " << m_status ;
	ERS_REPORT_IMPL( ers::warning, ers::Message, text.str(), );
      }
    }
  }
}

u_int TTCviModule::transform(const TTCvidal::TTCviCycleBase* const& b) {
  u_int r(0);
  if (const TTCvidal::TTCviCycleShort* s = m_confDB->cast<TTCvidal::TTCviCycleShort>(b)) {
    r = (0xff & s->get_Command()) << 23;
  }
  else if (const TTCvidal::TTCviCycleLong* l = m_confDB->cast<TTCvidal::TTCviCycleLong>(b)) {
    // encode the command:
    r = (0x1 << 31);
    r |= 0xff & l->get_Data();
    r |= ((0xff & l->get_Subaddress()) << 8);
    bool ext = (l->get_Registers()=="external") ? true : false;
    if (ext) {
      r |= (0x1 << 16);
    }
    r |= ((0x3fff & l->get_TTCrxAddress()) << 17);
  }
  return(r);
}

std::vector<u_int> TTCviModule::transform(const std::vector<const TTCvidal::TTCviCycleBase*>& v) {
  std::vector<u_int> r;
  for (std::vector<const TTCvidal::TTCviCycleBase*>::const_iterator it = v.begin();
       it != v.end();
       ++it) {
    r.push_back(this->transform(*it));
  }
  return r;
}



/******************************************************/
void TTCviModule::userCommand(std::string &commandName, std::string &argument)
/******************************************************/
{
  std::string sout = m_UID + "@TTCviModule::userCommand() ";
  ERS_LOG(sout << "Entered");

  ERS_LOG(sout << std::dec << "Received userCommand \"" << commandName.c_str() << "\" with argument \"" << argument << "\"");

  if (commandName == "COUNTER") {
    if (argument == "L1A") {
      ERS_LOG(sout << "Setting counter to L1A and reset");
      m_ttcvi->counterSelectionSet(TTCVI::CNT_L1A);
      m_ttcvi->counterReset();
    } else if (argument == "ORB") {
      ERS_LOG(sout << "Setting counter to Orbit and reset");
      m_ttcvi->counterSelectionSet(TTCVI::CNT_ORB);
      m_ttcvi->counterReset();
    } else {
      ERS_LOG(sout << "Unknown argument \"" << argument << "\" to command COUNTER. Leaving counter unchanged");
    }
  }
  else {
    ERS_LOG(sout << "UNKNOWN MESSAGE received");
  }

  ERS_LOG(sout << "Done");
}


extern "C" 
{
  extern ReadoutModule *createTTCviModule();
}

ReadoutModule *createTTCviModule()
{
  return (new TTCviModule());
}
