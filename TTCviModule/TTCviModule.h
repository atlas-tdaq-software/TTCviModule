#ifndef TTCVIMODULE_H
#define TTCVIMODULE_H

#include <string>
#include <vector>
#include <iostream>

#include "ROSCore/ReadoutModule.h"
#include "DFSubSystemItem/Config.h"
#include "config/Configuration.h"


// For debugging messages
#include "DFDebug/DFDebug.h"

#include "RCDTtc/RCDTtcvi.h"
#include "TTCvidal/TTCviBGoChannelBase.h"
#include "TTCvidal/TTCviBGoChannel.h"
#include "TTCvidal/TTCviBCR.h"
#include "TTCvidal/TTCviECR.h"

#include "TTCvidal/TTCviModule.h"
#include "TTCvidal/TTCviConfigBase.h"
#include "TTCvidal/TTCviConfig.h"
#include "TTCvidal/TTCviSimpleConfig.h"

#include "ipc/partition.h"
#include "is/info.h"



namespace RCD {

  using namespace ROS;

  class TTCviModule : public ReadoutModule {
  
  public:
    
    TTCviModule();
    virtual ~TTCviModule() noexcept;

    virtual const std::vector<DataChannel *> *channels();

    virtual void setup (DFCountedPointer<Config> configuration);
    
    void setup_TTCviConfig(const TTCvidal::TTCviConfig* ttcvi);
    void setup_TTCviSimpleConfig(const TTCvidal::TTCviSimpleConfig* ttcvi);

    virtual void clearInfo();
    virtual DFCountedPointer<Config> getInfo();

    virtual void configure(const daq::rc::TransitionCmd&);
    virtual void connect(const daq::rc::TransitionCmd&);
    virtual void prepareForRun(const daq::rc::TransitionCmd&);
    virtual void stopDC(const daq::rc::TransitionCmd&);

    virtual void disconnect(const daq::rc::TransitionCmd&);
    virtual void unconfigure(const daq::rc::TransitionCmd&);
    virtual void publish();
    virtual void publishFullStats();

    virtual void userCommand(std::string &commandName,
			     std::string &argument);

  private:
    void setup_BGoChannel(const TTCvidal::TTCviBGoChannel* bchan);
    void setup_BCR(const TTCvidal::TTCviBCR* bcr);
    void setup_BCR(unsigned int delay);
    void setup_ECR();

    void resetBGoChannelConfiguration();
    void resetConfiguration();
    void sendCycles(std::vector<u_int>& cycles);
    void dumpCycles(std::vector<u_int>& cycles);

    u_int transform(const TTCvidal::TTCviCycleBase* const&);
    std::vector<u_int> transform(const std::vector<const TTCvidal::TTCviCycleBase*>& v);

    unsigned int m_status;

    // for IS
    IPCPartition m_ipcpartition;
    daq::core::Partition* m_partition;
    Configuration* m_confDB;
    DFCountedPointer<Config> m_configuration;
    TTCvidal::TTCviModule* m_dbModule;

    std::string m_UID;
    std::string m_rcd_uid;
    TTCVI* m_ttcvi;
    u_int m_PhysAddress;
    std::string m_OperationMode;
 
    u_int m_OrbitInput;
    u_int m_CounterSelection;
    u_int m_L1AInput;
    u_int m_L1ARandomGeneratorFrequency;
    bool m_TriggerWordEnable;
    bool m_TriggerWordExternal;
    u_short m_TriggerWordAddress;
    u_short m_TriggerWordSubaddress;

    // BGO mode for Four BGO channels
    std::vector<u_int> m_Mode;
    std::vector<u_int> m_InhibitDelay;
    std::vector<u_int> m_InhibitDuration;
    std::vector<bool> m_Retransmit;
    std::vector<std::vector<u_int> > m_CycleList;

    // Some commands for the B-Go channels 
    TTCVI::LongCommand m_lcmd;
    TTCVI::ShortCommand m_scmd;

    std::vector<u_int> m_CyclesForConfigure;
    std::vector<u_int> m_CyclesForConnect;
    std::vector<u_int> m_CyclesForPrepareForRun;

    std::string m_IS_server;

    bool m_useConfiguration;
    bool m_useMonitoring;

    // Manufacturer ID, Board ID and Serial No. of the TTCvi module
    u_int m_manufacturerID;
    u_int m_boardID;
    u_int m_serialNo;

  };
  inline const std::vector<DataChannel *> *TTCviModule::channels() {
      return 0;
  }
}

#endif //  TTCVIMODULE_H
